package datastructure;

/**
 * @author keboom
 * @date 2021/3/13 15:50
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int x) {val = x;}
}
